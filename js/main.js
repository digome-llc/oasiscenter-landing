jQuery(function($) {
	"use strict";

	// Window Load
	$(window).load(function() {


		// Preloader
		$('.intro-tables, header').css('opacity', '0');
		$('.preloader').addClass('animated fadeOut').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$('.preloader').hide();
			$('header').addClass('animated fadeIn').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				$('.intro-tables').addClass('animated fadeInUp').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend');

				// // Wait for a sec then spread content
				// setTimeout(function(){
				// 	$('.intro-tables').addClass('loaded');
				// },500);

			});
		});


		// Dynamic height of cut thing
		$('#intro').height($(window).height() - 80);
		$('section .cut').each(function() {
			if ($(this).hasClass('cut-top'))
				$(this).css('border-right-width', $(this).parent().width() + "px");
			else if ($(this).hasClass('cut-bottom'))
				$(this).css('border-left-width', $(this).parent().width() + "px");
		});
	});


});



// Video controls
$("#video-controls").click(function() {
	console.log("hey you cliked it");

	// Play video
  var video = $("#video-promo").get(0);
  video.play();

  // Fade out overlay
  $(this).fadeOut();
  return false;

});



// Allow controls on hover
$('#video-promo').hover(function toggleControls() {
  if (this.hasAttribute("controls")) {
      this.removeAttribute("controls")
  } else {
      this.setAttribute("controls", "controls")
  }
});

